//
//  ServiceTypeExtensions.swift
//  Resolver
//
//  Created by Gregory Higley on 1/7/16.
//  Copyright © 2016 Prosumma LLC. All rights reserved.
//

import Foundation

// It's critical that this file is NOT added to the unit tests.
// The unit tests will have a different version returning a different type.

extension ServiceType {
    static func resolve(parameter: String) -> ServiceType {
        return Service(parameter: parameter)
    }
}
