//
//  Service.swift
//  Resolver
//
//  Created by Gregory Higley on 1/7/16.
//  Copyright © 2016 Prosumma LLC. All rights reserved.
//

import Foundation

class Service: ServiceType {
    let parameter: String
    init(parameter: String) {
        self.parameter = parameter
    }
    func performService() {
        print("I am the real service! \(parameter)")
    }
}