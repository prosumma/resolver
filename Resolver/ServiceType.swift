//
//  ServiceType.swift
//  Resolver
//
//  Created by Gregory Higley on 1/7/16.
//  Copyright © 2016 Prosumma LLC. All rights reserved.
//

import Foundation

protocol ServiceType {
    static func resolve(parameter: String) -> ServiceType
    func performService()
}