//
//  ViewController.swift
//  Resolver
//
//  Created by Gregory Higley on 1/7/16.
//  Copyright © 2016 Prosumma LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let service = Service.resolve("REAL!")
        service.performService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

