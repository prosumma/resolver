//
//  ResolverTests.swift
//  ResolverTests
//
//  Created by Gregory Higley on 1/7/16.
//  Copyright © 2016 Prosumma LLC. All rights reserved.
//

import XCTest
@testable import Resolver

class ResolverTests: XCTestCase {

    func testMockService() {
        // You have to call resolve on a concrete instance, not a protocol.
        // But it will still do the right thing.
        let service = Service.resolve("MOCKED!")
        service.performService()
    }
    
}
